package vfs.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import vfs.model.AccessScope;
import vfs.model.VFile;
import vfs.service.VFileService;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 14-1-7
 */
@Controller
public class ApiController {

    private static final Logger LOG = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private VFileService vFileService;

    private Set<String> keys;
    @Value("#{appConfig['limitRate']}")
    private int limitRate;

    @Value("#{appConfig['cacheSeconds']}")
    private int cacheSeconds;

    @Value("#{appConfig['keys']}")
    public void setKeys(String[] keys) {
        this.keys = Sets.newHashSet(keys);
    }

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public Map<String, Serializable> handleException(Exception ex, HttpServletRequest request) {
        LOG.debug("handle exception from request [" + request.getRequestURI() + "]", ex);
        Map<String, Serializable> map = Maps.newHashMap();
        map.put("ret", 0);
        map.put("msg", ex.toString());
        if (LOG.isDebugEnabled()) {
            map.put("detail", ExceptionUtils.getStackFrames(ex));
        }
        return map;
    }

    @RequestMapping("test")
    public String test() {
        return "test";
    }

    @RequestMapping("api/info/{id}")
    @ResponseBody
    public VFile info(@RequestParam(value = "token") String token,
                      @PathVariable("id") long id) {
        if (!vFileService.hasPermission(token, id)) {
            throw new RuntimeException("No permission");
        }
        return vFileService.get(id);
    }

    @RequestMapping("api/token")
    @ResponseBody
    public String token(@RequestParam(value = "id") Long[] ids,
                        @RequestParam(value = "ttl", required = false) Integer ttl,
                        @RequestParam(value = "key") String key) {
        if (!keys.contains(key)) {
            throw new RuntimeException("invalid Key");
        }
        return vFileService.createToken(Lists.newArrayList(ids), ttl);
    }

    @RequestMapping(value = "api/upload")
    @ResponseBody
    public VFile upload(@RequestParam(value = "token") String token,
                        @RequestParam(value = "description", required = false) String description,
                        @RequestParam(value = "scope", defaultValue = "TOKEN") AccessScope scope,
                        MultipartHttpServletRequest request) throws IOException {
        if (!vFileService.hasPermission(token, -1)) {
            throw new RuntimeException("No permission");
        }
        Iterator<String> it = request.getFileNames();
        while (it.hasNext()) {
            MultipartFile mFile = request.getFile(it.next());
            if (!mFile.isEmpty() && mFile.getOriginalFilename().toLowerCase().endsWith(".mp4")) {
                VFile vf = new VFile();
                vf.setName(mFile.getOriginalFilename());
                vf.setDescription(StringUtils.trimToNull(description));
                vf.setScope(scope);
                vFileService.saveFile(vf, mFile);
                return vf;
            }
        }
        throw new RuntimeException("No upload file found");
    }

    @RequestMapping("api/remove")
    @ResponseBody
    public Map remove(@RequestParam(value = "id") Long[] ids,
                      @RequestParam(value = "token") String token) {
        for (Long id : ids) {
            if (vFileService.hasPermission(token, id)) {
                vFileService.remove(Collections.singletonList(id));
            }
        }
        return Collections.singletonMap("ret", 1);
    }

    @RequestMapping("s/{token}/{id}.{ext}")
    @ResponseBody
    public void stream(
            @PathVariable("token") String token,
            @PathVariable("id") String idString,
            @PathVariable("ext") String ext,
            HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        long id = Long.parseLong(StringUtils.substringBefore(idString, "-"));
        VFile vf = vFileService.get(id);
        switch (vf.getScope()) {
            case ALL:
                break;
            case TOKEN:
                if (!vFileService.hasPermission(token, id)) {
                    throw new RuntimeException("No permission");
                }
                break;
        }

        if (!checkModified(vf.getUpdateAt().getTime(), request, response)) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            return;
        }

        response.setHeader("X-Accel-Redirect", "/" + StringUtils.substringBeforeLast(vf.getPath(), "/") + "/" + idString + "." + ext);
        if (limitRate > 0) {
            response.setIntHeader("X-Accel-Limit-Rate", limitRate);
        }
        if (cacheSeconds > 0) {
            response.setDateHeader("Expires", System.currentTimeMillis() + cacheSeconds * 1000L);
            String headerValue = "max-age=" + cacheSeconds;
            response.setHeader("Cache-Control", headerValue);
        }
    }

    @RequestMapping("f/{id}")
    @ResponseBody
    public void dl(@PathVariable("id") long id,
                   @RequestParam(value = "token") String token, HttpServletRequest request, HttpServletResponse response) throws IOException {
        VFile vf = vFileService.get(id);
        switch (vf.getScope()) {
            case ALL:
                break;
            case TOKEN:
                if (!vFileService.hasPermission(token, id)) {
                    throw new RuntimeException("No permission");
                }
                break;
        }

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; " + getEncodeFileName(request, vf.getName()));
        response.setContentLength(vf.getSize());
        IOUtils.copy(vFileService.getInputStream(vf), response.getOutputStream());
    }

    private boolean checkModified(Long modified, HttpServletRequest request, HttpServletResponse response) {
        if ("GET".equals(request.getMethod())) {
            if (modified == null)
                return true;
            String etag = String.valueOf(modified);
            String ifNoneMatch = request.getHeader("If-None-Match");
            if (StringUtils.isNotEmpty(ifNoneMatch) && etag.equals(ifNoneMatch)) {
                return false;
            }
            long ifModifiedSince = request.getDateHeader("If-Modified-Since");
            if (ifModifiedSince > 0L) {
                long modDate = (modified / 1000L) * 1000L;
                if (modDate <= ifModifiedSince) {
                    return false;
                }
            }
            response.setDateHeader("Last-Modified", modified);
            response.setHeader("ETag", etag);
        }
        return true;
    }

    private static String getEncodeFileName(HttpServletRequest request, String fileName) {
        String userAgent = StringUtils.lowerCase(request.getHeader("User-Agent"));
        try {
            String efn = URLEncoder.encode(fileName, "UTF-8");
            String defName = "filename=\"" + efn + "\"";
            if (StringUtils.isEmpty(userAgent) || userAgent.contains("msie")) {
                return defName;
            } else if (userAgent.contains("safari")) {
                return "filename=\"" + new String(fileName.getBytes("UTF-8"), "ISO8859-1") + "\"";
            } else if (userAgent.contains("applewebkit")) {
                return "filename=\"" + MimeUtility.encodeText(fileName, "UTF8", "B") + "\"";
            } else if (userAgent.contains("opera") || userAgent.contains("mozilla")) {
                return "filename*=UTF-8''" + efn;
            } else {
                return defName;
            }
        } catch (UnsupportedEncodingException ignored) {
        }
        return fileName;
    }
}
