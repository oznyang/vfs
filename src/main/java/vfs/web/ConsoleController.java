package vfs.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vfs.service.VFileService;

import java.util.Collections;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 14-1-7
 */
@Controller
@RequestMapping(value = "console")
public class ConsoleController {

    @Autowired
    private VFileService vFileService;

    @RequestMapping(value = "file", method = RequestMethod.GET)
    public String fileIndex(Model model) {
        model.addAttribute("files", vFileService.all());
        return "file";
    }

    @RequestMapping(value = "file/play", method = RequestMethod.GET)
    public String filePlay(@RequestParam(value = "id") long id, Model model) {
        model.addAttribute("token", vFileService.createToken(id, null));
        return "play";
    }

    @RequestMapping(value = "file/remove", method = RequestMethod.GET)
    public String fileRemove(@RequestParam(value = "id") long id) {
        vFileService.remove(Collections.singletonList(id));
        return "redirect:/console/file";
    }
}
