package vfs.model;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 14-1-7
 */
@Entity
@Table(name = "t_vfile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class VFile implements Serializable {

    public static final Set<String> VIDEO_POSTFIX = new HashSet<String>(Arrays.asList(new String[]{"mpg", "mpeg", "mpe", "avi", "mov", "asf", "mp4", "wmv", "flv", "3gp"}));
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 32)
    private Long id;
    @Column(length = 256)
    private String name;
    @Column(length = 256, nullable = false)
    @JSONField(serialize = false)
    private String path = "";
    @Column(name = "description", length = 1024)
    private String description;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updateAt;
    private int size;
    @Column(nullable = false)
    @JSONField(serialize = false)
    private AccessScope scope = AccessScope.ALL;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public AccessScope getScope() {
        return scope;
    }

    public void setScope(AccessScope scope) {
        this.scope = scope;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
