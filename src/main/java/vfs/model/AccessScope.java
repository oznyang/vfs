/*
 * Project:  any
 * Module:   any-file-store
 * File:     AccessScope.java
 * Modifier: xyang
 * Modified: 2012-07-04 10:30
 *
 * Copyright (c) 2012 Sanyuan Ltd. All Rights Reserved.
 *
 * Copying of this document or code and giving it to others and the
 * use or communication of the contents thereof, are forbidden without
 * expressed authority. Offenders are liable to the payment of damages.
 * All rights reserved in the event of the grant of a invention patent or the
 * registration of a utility model, design or code.
 */
package vfs.model;


/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-7-1
 */
public enum AccessScope {
    ALL("所有人"),
    USER("登录用户"),
    TOKEN("token认证"),
    SELF("仅自己可访问"),
    SESSION("需要会话授权才能访问");
    private String label;

    AccessScope(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
