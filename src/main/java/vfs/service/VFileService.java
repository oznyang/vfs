package vfs.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import vfs.model.VFile;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 14-1-7
 */
public interface VFileService {

    String createToken(long id, Integer ttl);

    String createToken(Collection<Long> ids, Integer ttl);

    boolean hasPermission(String token, long id);

    VFile save(VFile vFile);

    void remove(Collection<Long> ids);

    VFile get(long id);

    List<VFile> all();

    VFile saveFile(VFile vFile, MultipartFile mpf);

    InputStream getInputStream(VFile vFile);
}
