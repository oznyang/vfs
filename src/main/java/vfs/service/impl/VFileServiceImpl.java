package vfs.service.impl;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import vfs.model.VFile;
import vfs.repo.VFileRepo;
import vfs.service.VFileService;

import java.io.*;
import java.util.*;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 14-1-7
 */
public class VFileServiceImpl implements VFileService {
    private static final Logger LOG = LoggerFactory.getLogger(VFileServiceImpl.class);

    private FastDateFormat PATH_FORMATER = FastDateFormat.getInstance("yy/MM/dd");

    @Autowired
    private VFileRepo vFileRepo;

    private AsyncTaskExecutor executor;
    private String segmentCmd = "ffmpeg -y -i %s -c copy -map 0 -vbsf h264_mp4toannexb -f segment -segment_time 10 -segment_list %s.m3u8 -segment_format mpegts %s-%%03d.ts";
    private Ehcache tokenCache;
    private File rootPath;
    private String savePath;

    public void setExecutor(AsyncTaskExecutor executor) {
        this.executor = executor;
    }

    public void setSegmentCmd(String segmentCmd) {
        this.segmentCmd = segmentCmd;
    }

    public void setTokenCache(Ehcache tokenCache) {
        this.tokenCache = tokenCache;
    }

    public void setRootPath(File rootPath) {
        this.rootPath = rootPath;
    }

    public void setSavePath(String subPath) {
        this.savePath = subPath;
    }

    @Override
    public String createToken(long id, Integer ttl) {
        return createToken(Collections.singletonList(id), ttl);
    }

    @Override
    public String createToken(Collection<Long> ids, Integer ttl) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new IllegalArgumentException("Need id");
        }
        String token = RandomStringUtils.randomAlphanumeric(10);
        tokenCache.put(new Element(token, ids.size() == 1 ? ids.iterator().next() : new HashSet<Long>(ids), null, null, ttl));
        return token;
    }

    @Override
    public boolean hasPermission(String token, long id) {
        Element el = tokenCache.get(token);
        if (el != null) {
            Object v = el.getObjectValue();
            if (v instanceof Long) {
                return (Long) v == id;
            } else {
                return ((Set) v).contains(id);
            }
        }
        return false;
    }

    @Override
    @Transactional
    public VFile save(VFile vFile) {
        vFile.setUpdateAt(new Date());
        vFileRepo.save(vFile);
        return vFile;
    }

    @Override
    @Transactional
    public void remove(Collection<Long> ids) {
        if (ids != null) {
            for (long id : ids) {
                VFile vf = get(id);
                File file = new File(rootPath, vf.getPath());
                if (file.exists()) {
                    try {
                        FileUtils.deleteDirectory(file.getParentFile());
                    } catch (IOException ignored) {
                    }
                }
                vFileRepo.delete(vf);
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public VFile get(long id) {
        VFile vFile = vFileRepo.findOne(id);
        if (vFile == null) {
            throw new IllegalStateException("File with id [" + id + "] not found");
        }
        return vFile;
    }

    @Override
    @Transactional(readOnly = true)
    public List<VFile> all() {
        return vFileRepo.findAll(new Sort(Sort.Direction.DESC, "id"));
    }

    @Override
    @Transactional
    public VFile saveFile(VFile vFile, MultipartFile mpf) {
        save(vFile);
        String path = savePath + "/" + PATH_FORMATER.format(new Date()) + "/" + vFile.getId() + "/" + vFile.getId() + ".mp4";
        File file = new File(rootPath, path);
        File parentPath = file.getParentFile();
        if (!parentPath.exists()) {
            parentPath.mkdirs();
        }
        try {
            mpf.transferTo(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        vFile.setSize((int) file.length());
        vFile.setPath(path);
        save(vFile);
        segmentVideo(file);
        return vFile;
    }

    private void segmentVideo(final File src) {
        final String destName = StringUtils.substringBeforeLast(src.getName(), ".");
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    runCmd(String.format(segmentCmd, src.getName(), destName, destName), src.getParentFile());
                } catch (IOException e) {
                    LOG.warn("Segment video [{}] error, {}", src.getAbsolutePath(), e.getMessage());
                }
            }
        });
    }

    @Override
    @Transactional(readOnly = true)
    public InputStream getInputStream(VFile vFile) {
        File file = new File(rootPath, vFile.getPath());
        try {
            return FileUtils.openInputStream(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String runCmd(String cmd, File dir) throws IOException {
        LOG.info("Execute command:[{}]", cmd);
        Writer out = new StringWriter();
        IOUtils.copy(Runtime.getRuntime().exec(cmd, null, dir).getInputStream(), out);
        String s = out.toString();
        LOG.debug("Output: [{}]", s);
        return s;
    }
}
