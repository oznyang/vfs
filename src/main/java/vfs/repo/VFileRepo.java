package vfs.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import vfs.model.VFile;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oznyang@163.com">oznyang</a>
 * @version V1.0, 13-12-24
 */
public interface VFileRepo extends JpaRepository<VFile, Long> {

    Page<VFile> findByNameLike(String name, Pageable p);
}
