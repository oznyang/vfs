var apis = [
    '文件',
    {
        name:'创建token',
        url:'token',
        method:'post',
        params:[
            {name:'key', code:'key',def:'111'},
            {name:'文件id,可多个', code:'id', def:'-1'}
        ]
    },
    {
        name:'获取文件信息',
        url:'info/{id}',
        method:'post',
        params:[
            {name:'token', code:'token'}
        ]
    },
    {
        name:'上传文件',
        url:'upload',
        method:'post',
        params:[
            {name:'token', code:'token'},
            {name:'说明', code:'description'},
            {name:'访问权限级别', code:'scope'},
            {name:'文件', code:'file', type:'file'}
        ]
    },
    {
        name:'根据id删除文件',
        url:'remove',
        method:'post',
        params:[
            {name:'token', code:'token'},
            {name:'文件id,可多个', code:'id', def:'1'}
        ]
    }
];
function getType(object) {
    var _t;
    return ((_t = typeof(object)) == "object" ? object == null && "null" || Object.prototype.toString.call(object).slice(8, -1) : _t).toLowerCase();
}
function choiceApi(index) {
    var api = apis[index];
    var apiform = document.getElementById('api-form');
    $('#api-url span.name').html(api.name);
    apiForm._url.value = ctx + (api.url.indexOf('/') == 0 ? api.url : '/api/' + api.url);
    apiForm._method.value = api.method;
    var arr = [];
    if (api.params) {
        for (var i = 0; i < api.params.length; i++) {
            var param = api.params[i];
            arr.push('<li>');
            if ('file' == api.params[i].type) {
                arr.push(param.name + '-' + param.code + '&nbsp;&nbsp;<input type="file" class="f" name="' + param.code + '" value="' + (param.def != undefined ? param.def : '') + '" size="50"/>');
            } else if ('checkbox' == api.params[i].type) {
                var arr1 = "";
                for (var j = 0; j < param.values.length; j++) {
                    var chkapi = param.values[j];
                    arr1 = arr1 + '<input type="checkbox" name="' + param.code + '" value="' + chkapi.value + '" />' + chkapi.name;
                }
                arr.push(param.name + '-' + param.code + '&nbsp;&nbsp;' + arr1);
            }
            else if ('select' == api.params[i].type) {
                var arr1 = "";
                for (var j = 0; j < param.values.length; j++) {
                    var chkapi = param.values[j];
                    arr1 = arr1 + '<option  value="' + chkapi.value + '">' + chkapi.name + '</option>';
                }
                arr.push(param.name + '-' + param.code + '&nbsp;&nbsp; <select name="' + param.code + '" class="ipt validate[required]">' + arr1 + '</select>');
            }
            else {
                arr.push(param.name + '-' + param.code + '&nbsp;&nbsp;<input type="text" name="' + param.code + '" value="' + (param.def != undefined ? param.def : '') + '" size="50"/>');
            }
            if (param.doc) {
                arr.push(param.doc);
            }
            arr.push('</li>');
        }
    }
    $('#api-params').html(arr.join(''));
    $("#api-result").val('');
}
function appendSid(a, url) {
    var sid = $('#api-ticket').html();
    window.open(a.href + '&url=' + encodeURIComponent(url + '?sid=' + sid));
    return false;
}
function doSubmit(form) {
    var url = form._url.value+'?debug';
    var file = $('#api-params input.f');
    if (file.val()) {
        form.action = url;
        form.submit();
        return;
    }
    $.ajax({
        type:"POST",
        dataType:'text',
        url:url,
        data:$("#api-form").serialize(),
        success:function (data) {
            $("#api-result").val(js_beautify(data));
        }
    });
}
var api, apiForm, url = location.href;
url = url.substring(0, url.indexOf('/', 8));
$(function () {
    var arr = [];
    for (var i = 0; i < apis.length; i++) {
        var _api = apis[i];
        if (getType(_api) == 'string') {
            arr.push('<li class="title"><h3>' + _api + '</h3></li>');
        } else {
            arr.push('<li><a href="javascript:choiceApi(' + i + ')">' + _api.name + '</a></li>');
        }
    }
    $('#api-list').append(arr.join(''));
    apiForm = document.getElementById('api-form');
    choiceApi(1);
});