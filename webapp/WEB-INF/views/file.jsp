<%@ page pageEncoding="UTF-8" %>
<%@ include file="common/header.jsp" %>
<table class="list-table" style="width: 100%;">
    <caption>
        <a href="${ctx}/test" class="a-btn" target="_blank">api测试</a>
    </caption>
    <tr>
        <th>Id</th>
        <th>名称</th>
        <th>更新时间</th>
        <th>scope</th>
        <th style="width:180px;">操作</th>
    </tr>
    <c:forEach items="${files}" var="f">
        <tr>
            <td>${f.id}</td>
            <td>${f.name}</td>
            <td><fmt:formatDate value="${f.updateAt}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            <td>${f.scope.label}</td>
            <td>
                <a href="file/play?id=${f.id}" target="_blank">播放</a>&nbsp;
                <a href="file/remove?id=${f.id}" onclick="return confirm('确定要删除吗?')">删除</a>&nbsp;
            </td>
        </tr>
    </c:forEach>
</table>
<%@ include file="common/footer.jsp" %>