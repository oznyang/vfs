<%@ page pageEncoding="UTF-8" %>
<%@ include file="common/header.jsp" %>
<script src="${ctx}/static/js/jwplayer/jwplayer.js" type="text/javascript"></script>
<div id="mediaplayer">JW Player goes here</div>
<script type="text/javascript">
    jwplayer("mediaplayer").setup({
        flashplayer: "${ctx}/static/js/jwplayer/jwplayer.flash.swf",
        file: "${ctx}/s/${token}/${param.id}.m3u8",
        autostart: true,
        provider: 'http',
        controlbar: 'over',
        width: '800',
        height: '500'
    });
</script>
<%@ include file="common/footer.jsp" %>