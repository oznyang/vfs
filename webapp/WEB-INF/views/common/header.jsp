<%@ page pageEncoding="UTF-8" %>
<%@ include file="includes.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>视频文件管理</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <link href="${ctx}/static/css/main.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx}/static/css/console.css" type="text/css" media="screen" rel="stylesheet"/>
    <link href="${ctx}/static/css/jquery.validationEngine.css" type="text/css" media="screen" rel="stylesheet"/>
    <script src="${ctx}/static/js/jquery-min.js" type="text/javascript"></script>
    <script src="${ctx}/static/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="${ctx}/static/js/console.js" type="text/javascript"></script>
    <script type="text/javascript">var ctx='${ctx}';</script>
</head>
<body>
<div id="container">
    <div id="tbody">